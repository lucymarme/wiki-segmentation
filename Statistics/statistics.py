import json
import pandas as pd

df = pd.read_csv('/home/lmarme/cleanCSV/clean_9.csv')

del df['number of sections per document']
del df['number of words per paragraph']


df['average number of words per paragraph (per document)'] = ""
df['number of sections per document'] = ""
df['number of words per document'] = ""
df['number of words per paragraph'] = ""
df['number of paragraphs per section'] = ""

amount_words = []
paragraphCounter = 0
WordsParagraphDocument = 0
numberWordsDocument = 0
documentCount = 0
numberParagraphs = 0
wordsParagraph = 0
w = 0
for i, row in df.iterrows():
    try:
        if pd.isnull(df['paragraph'].iloc[i]):
            if paragraphCounter > 0:
                numberWordsDocument = len(amount_words)
                WordsParagraphDocument = numberWordsDocument / paragraphCounter
                # print("number of words per document" +
                #       ' ' + str(numberWordsDocument))
                # print("average number of words per paragraph (per document)" +
                #       ' ' + str(WordsParagraphDocument))
                df.iloc[i - paragraphCounter, 2] = str(WordsParagraphDocument)
                df.iloc[i - paragraphCounter, 4] = str(numberWordsDocument)
                paragraphCounter = 0
            i = i + 2
            documentCount = documentCount + 1
            w = 1
        else:
            paragraphCounter = paragraphCounter + 1
            amount_words_string = ""
            amount_words_string = df.iloc[i, 1]
            amount_words_temp = amount_words_string.split()

            for x in range(0, len(amount_words_temp)):
                amount_words.append(amount_words_temp[x])

            if w != 1:
                wordsParagraph = len(amount_words_temp)
                df.iloc[i, 5] = str(wordsParagraph)
            else:
                w = 0

    except Exception as e:
        pass

numberParagraphs = df[df.columns[0]].count() - documentCount
average = len(amount_words) / numberParagraphs

print("total number of words:" + ' ' + str(len(amount_words)))
print("number of paragraph:" + ' ' + str(numberParagraphs))
print("average number of words per paragraph:" + ' ' + str(average))


sectionCount = 0
totalSections = 0
averageNumberSections = 0
NumberParagraphsDocument = 0
d = 0
p = 0
paragraphSection = 0
s = 0
sectionString = ""
sectionList = ['startwert']
for i, row in df.iterrows():
    try:
        if pd.isnull(df['paragraph'].iloc[i]):
            d = d + 1
            try:
                df.iloc[i - p, 3] = str(sectionCount)
            except Exception as e:
                pass
            totalSections = totalSections + sectionCount
            sectionCount = 0
            i = i + 2
            p = 0
        else:
            p = p + 1
            sectionString = ""
            sectionString = df.iloc[i, 0]
            sectionList.append(sectionString)
            paragraphSection = paragraphSection + 1
            s = s + 1
            if sectionList[len(sectionList) - 1] != sectionList[len(sectionList) - 2]:
                sectionCount = sectionCount + 1
                df.iloc[i - s, 6] = str(paragraphSection)
                s = 0
                paragraphSection = 0

    except Exception as e:
        pass

averageNumberSections = totalSections / d
print("average number sections per document" + ' ' + str(averageNumberSections))

totalParagraphs = df[df.columns[0]].count() - d
averageNumberParagraphs = totalParagraphs / totalSections
print("average number paragraphs per section" +
      ' ' + str(averageNumberParagraphs))

NumberParagraphsDocument = totalParagraphs / d
print("average number paragraphs per document" +
      ' ' + str(NumberParagraphsDocument))


h2 = []
h3 = []
no_title = []
title_list = []
count_h2 = 0
count_h3 = 0
count_no_title = 0
for i, row in df.iterrows():

    title_string = ""
    title_string = df.iloc[i, 0]
    title_list.append(title_string)

    try:
        if title_string == '[]':
            no_title.append(title_string)
        if title_string == '':
            pass
        if '::' in title_string:
            h3.append(title_string.split('::')[1])

    except Exception as e:
        pass

count_no_title = len(no_title) - documentCount
count_h2 = df[df.columns[0]].count() - count_no_title
count_h3 = len(h3)
print("number of paragraphs with <h2> tag:" + ' ' + str(count_h2))
print("number of paragraphs with <h3> tag:" + ' ' + str(count_h3))
print("number of paragraphs with no tag (general section):" +
      ' ' + str(count_no_title))

df.to_csv('/home/lmarme/clean_9_Stats.csv', index=False)
