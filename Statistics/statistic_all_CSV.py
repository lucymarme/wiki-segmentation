import json
import pandas as pd
from pathlib import Path
import os
from os import listdir
from os.path import isfile, join
import ast

### values for entire dataset ###

numberParagraphs_total = 0
amount_words_total = 0

average_total = 0
averageNumberSections_total = 0
averageNumberParagraphs_total = 0
NumberParagraphsDocument_total = 0

for files in os.listdir('/home/lmarme/csvFiles/'):

    filename = os.fsdecode('/home/lmarme/csvFiles/' + files)
    df = pd.read_csv(filename)
    df['average number of words per paragraph (per document)'] = ""
    df['number of sections per document'] = ""
    df['number of words per document'] = ""
    df['number of words per paragraph'] = ""
    df['number of paragraphs per section'] = ""

    amount_words = []
    paragraphCounter = 0
    WordsParagraphDocument = 0
    numberWordsDocument = 0
    documentCount = 0
    numberParagraphs = 0
    wordsParagraph = 0
    w = 0
    for i, row in df.iterrows():
        try:
            if pd.isnull(df['paragraph'].iloc[i]):
                if paragraphCounter > 0:
                    numberWordsDocument = len(amount_words)
                    WordsParagraphDocument = numberWordsDocument / paragraphCounter
                    # print("number of words per document" +
                    #       ' ' + str(numberWordsDocument))
                    # print("average number of words per paragraph (per document)" +
                    #       ' ' + str(WordsParagraphDocument))
                    df.iloc[i - paragraphCounter,
                            2] = str(WordsParagraphDocument)
                    df.iloc[i - paragraphCounter, 4] = str(numberWordsDocument)
                    paragraphCounter = 0
                i = i + 2
                documentCount = documentCount + 1
                w = 1
            else:
                paragraphCounter = paragraphCounter + 1
                amount_words_string = ""
                amount_words_string = df.iloc[i, 1]
                amount_words_temp = amount_words_string.split()

                for x in range(0, len(amount_words_temp)):
                    amount_words.append(amount_words_temp[x])

                if w != 1:
                    wordsParagraph = len(amount_words_temp)
                    df.iloc[i, 5] = str(wordsParagraph)
                else:
                    w = 0

        except Exception as e:
            pass

    numberParagraphs = df[df.columns[0]].count() - documentCount
    average = len(amount_words) / numberParagraphs

    # adding number of words/paragraphs per CSV to total
    amount_words_total = amount_words_total + len(amount_words)
    numberParagraphs_total = numberParagraphs_total + numberParagraphs
    average_total = average_total + average

    print("total number of words (" + files + "):" + ' ' + str(len(amount_words)))
    print("number of paragraphs (" + files + "):" + ' ' + str(numberParagraphs))
    print("average number of words per paragraph:" + ' ' + str(average))

    sectionCount = 0
    totalSections = 0
    averageNumberSections = 0
    NumberParagraphsDocument = 0
    d = 0
    p = 0
    paragraphSection = 0
    s = 0
    sectionString = ""
    sectionList = ['startwert']
    for i, row in df.iterrows():
        try:
            if pd.isnull(df['paragraph'].iloc[i]):
                d = d + 1
                try:
                    df.iloc[i - p, 3] = str(sectionCount)
                except Exception as e:
                    pass
                totalSections = totalSections + sectionCount
                sectionCount = 0
                i = i + 2
                p = 0
            else:
                p = p + 1
                sectionString = ""
                sectionString = df.iloc[i, 0]
                sectionList.append(sectionString)
                paragraphSection = paragraphSection + 1
                s = s + 1
                if sectionList[len(sectionList) - 1] != sectionList[len(sectionList) - 2]:
                    sectionCount = sectionCount + 1
                    df.iloc[i - s, 6] = str(paragraphSection)
                    s = 0
                    paragraphSection = 0

        except Exception as e:
            pass

    averageNumberSections = totalSections / d
    print("average number sections per document (" +
          files + ")" + ' ' + str(averageNumberSections))

    totalParagraphs = df[df.columns[0]].count() - d
    averageNumberParagraphs = totalParagraphs / totalSections
    print("average number paragraphs per section (" + files + ")" +
          ' ' + str(averageNumberParagraphs))

    NumberParagraphsDocument = totalParagraphs / d
    print("average number paragraphs per document (" + files + ")" +
          ' ' + str(NumberParagraphsDocument))

    # calculating total results
    averageNumberSections_total = averageNumberSections_total + averageNumberSections
    averageNumberParagraphs_total = averageNumberParagraphs_total + averageNumberParagraphs
    NumberParagraphsDocument_total = NumberParagraphsDocument_total + \
        NumberParagraphsDocument

average_total_adapted = 0
averageNumberSections_total_adapted = 0
averageNumberParagraphs_total_adapted = 0
NumberParagraphsDocument_total_adapted = 0

print("total number of words:" + ' ' + str(amount_words_total))
print("total number of paragraphs:" + ' ' + str(numberParagraphs_total))

average_total_adapted = average_total / 74
print("average number of words per paragraph (in total):" +
      ' ' + str(average_total_adapted))


averageNumberSections_total_adapted = averageNumberSections_total / 74
print("average number sections per document (in total)" +
      ' ' + str(averageNumberSections_total_adapted))


averageNumberParagraphs_total_adapted = averageNumberParagraphs_total / 74
print("average number paragraphs per section (in total)" +
      ' ' + str(averageNumberParagraphs_total_adapted))

NumberParagraphsDocument_total_adapted = NumberParagraphsDocument_total / 74
print("average number paragraphs per document (in total)" +
      ' ' + str(NumberParagraphsDocument_total_adapted))
