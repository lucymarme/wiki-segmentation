import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

df = pd.read_csv(
    '/Users/lucymarme/Desktop/Fort_Praktikum/clean_9_Stats.csv')

value_for_bins = []

for i, row in df.iterrows():
    # only for the clean_9_Stats.csv because there is always a 1 in the empty line between pages
    # if pd.isnull(df['title']).iloc[i] or pd.isnull(df['number of words per paragraph']).iloc[i]:
    if pd.isnull(df['title']).iloc[i] or pd.isnull(df['number of paragraphs per section']).iloc[i]:
        pass
    else:
        # for paragraph lengths
        # klexikon_number_sentences.append(df.iloc[i, 5])
        value_for_bins.append(df.iloc[i, 6])

# This calculates a bin width of 4: (max_range - min_range) / bins
plt.hist(value_for_bins, bins=range(0, 15), color='#1b9e77')
# Manual axis restriction
plt.xlim([0, 15])
plt.ylim([0, 20000])
# Plotting of additional indicators in plot
plt.axvline(np.mean(value_for_bins),
            color='k', linestyle='dashed', linewidth=2)
plt.axvline(np.mean(value_for_bins) -
            np.std(value_for_bins), color='k', linestyle='dotted', linewidth=1)
plt.axvline(np.mean(value_for_bins) +
            np.std(value_for_bins), color='k', linestyle='dotted', linewidth=1)
plt.axvline(np.median(value_for_bins),
            color='#d95f02', linestyle='solid', linewidth=2)
print(np.mean(value_for_bins))

print(np.median(value_for_bins))
# Adjust file path if necessary
plt.savefig("histogram_afterCleaning_PLOT_ParagraphPerSection_V5.png")
plt.show()
plt.close()
