import math
import random

with open('/Users/lucymarme/Desktop/Fort_Praktikum/segmentation.txt', 'r') as s:

    # splitting .txt file into paragraphs
    segmentation_file = s.read()
    paragraph_list = segmentation_file.split('\n')

    # removing titles and title tags
    for paragraphs in paragraph_list:
        if '<h2>' in paragraphs:
            paragraph_list.remove(paragraphs)
        if '<h3>' in paragraphs:
            paragraph_list.remove(paragraphs)
        if '<h4>' in paragraphs:
            paragraph_list.remove(paragraphs)

    print("length paragraph_list = " + str(len(paragraph_list)))

    # generating randomly one or zero and then formating the output .txt file
    with open('/Users/lucymarme/Desktop/Fort_Praktikum/segmentation_output_test.txt', 'w') as output:

        i = 0
        n = 0  # comparison counter
        output.write(paragraph_list[0])
        for paragraphs in paragraph_list:

            random_value = random.random()
            rounded_value = round(random_value)

            if i < len(paragraph_list) - 2 and rounded_value == 1:
                output.write('\n' + '\n' + paragraph_list[i + 1])
                n = n + 1

            if i < len(paragraph_list) - 2 and rounded_value == 0:
                output.write('\n' + '================' +
                             '\n' + paragraph_list[i + 1])
                n = n + 1
            i = i + 1

print("i = " + str(i))
print("n = " + str(n))
