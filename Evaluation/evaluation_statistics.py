import json

number_pages_total = 0
number_mistakes_total = 0
number_mistakes_per_page = 0
average_mistakes_per_page = 0

##### calculating average number if mistakes per document #####
with open('/Users/lucymarme/Desktop/Fort_Praktikum/slow-bert-og-test-results/num_mistakes_per_file.json', 'r') as json_object:
    data = json.load(json_object)
    for key in data:
        number_pages_total = number_pages_total + 1
        number_mistakes_per_page = data[key]
        number_mistakes_total = number_mistakes_total + number_mistakes_per_page

    average_mistakes_per_page = number_mistakes_total / number_pages_total

    print("Number of pages in total:" + ' ' + str(number_pages_total))
    print("Number of mistakes in total" + ' ' + str(number_mistakes_total))
    print("average mistakes per page" + ' ' + str(average_mistakes_per_page))


##### calculating k = 0,...,6 shares #####

k0_amount = 0
k1_amount = 0
k2_amount = 0
k3_amount = 0
k4_amount = 0
k5_amount = 0
k6_amount = 0

with open('/Users/lucymarme/Desktop/Fort_Praktikum/slow-bert-og-test-results/num_mistakes_per_file.json', 'r') as json_object:
    data = json.load(json_object)
    for key in data:

        if data[key] == 0:
            k0_amount = k0_amount + 1

        if data[key] <= 1:
            k1_amount = k1_amount + 1

        if data[key] <= 2:
            k2_amount = k2_amount + 1

        if data[key] <= 3:
            k3_amount = k3_amount + 1

        if data[key] <= 4:
            k4_amount = k4_amount + 1

        if data[key] <= 5:
            k5_amount = k5_amount + 1

        if data[key] <= 6:
            k6_amount = k6_amount + 1

    k0_share = k0_amount / number_pages_total
    k1_share = k1_amount / number_pages_total
    k2_share = k2_amount / number_pages_total
    k3_share = k3_amount / number_pages_total
    k4_share = k4_amount / number_pages_total
    k5_share = k5_amount / number_pages_total
    k6_share = k6_amount / number_pages_total

    print("k = 0" + ' ' + str(k0_share))
    print("k = 1" + ' ' + str(k1_share))
    print("k = 2" + ' ' + str(k2_share))
    print("k = 3" + ' ' + str(k3_share))
    print("k = 4" + ' ' + str(k4_share))
    print("k = 5" + ' ' + str(k5_share))
    print("k = 6" + ' ' + str(k6_share))


##### calculating accuracy #####
amount_predictions_document = 0
total_amount_predictions_document = 0
negative_accuracy = 0
positive_accuracy = 0
with open('/Users/lucymarme/Desktop/Fort_Praktikum/slow-bert-og-test-results/model_prediction_dictionary.json', 'r') as json_prediction:
    data = json.load(json_prediction)
    for key in data:
        prediction_list = data[key]
        amount_predictions_document = len(prediction_list)
        total_amount_predictions_document = total_amount_predictions_document + \
            amount_predictions_document

    print("Total amount of predictions" + ' ' +
          str(total_amount_predictions_document))

    negative_accuracy = number_mistakes_total / total_amount_predictions_document
    positive_accuracy = 1 - negative_accuracy

    print("negative accuracy:" + ' ' + str(negative_accuracy))
    print("positive accuracy:" + ' ' + str(positive_accuracy))
