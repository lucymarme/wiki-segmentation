[
    {
        "Text": "Calestous Juma:2870408\n",
        "Section": "[]"
    },
    {
        "Text": "Calestous Juma (June 9, 1953 \u2013 December 15, 2017) was a Kenyan scientist and academic specialising in sustainable development. He was named one of the most influential 100 Africans in 2012, 2013 and 2014 by the \"New African\" magazine. He was Professor of the Practice of International Development and Faculty Chair of the Innovation for Economic Development Executive Program at Harvard Kennedy School. Juma was Director of the School's Science, Technology and Globalization Project at Harvard Kennedy School as well as the Agricultural Innovation in Africa Project funded by the Bill and Melinda Gates Foundation. His last book, \"Innovation and Its Enemies: Why People Resist New Technologies.\" was published by Oxford University Press in 2016.\n",
        "Section": "[]"
    },
    {
        "Text": "In recognition of his work, Juma had been elected to the Royal Society of London, the US National Academy of Sciences, Third World Academy of Sciences (TWAS), the UK Royal Academy of Engineering, the African Academy of Sciences and the New York Academy of Sciences.\n",
        "Section": "[]"
    },
    {
        "Text": "Juma grew up on the Kenyan shores of Lake Victoria where he obtained early education as one of the pioneer students of the then Port Victoria Secondary School (now John Osogo SS) from 1968-1971. He first worked as an elementary school teacher before becoming Africa's first science and environment journalist at Kenya's \"Daily Nation\" newspaper. Juma later joined the Nairobi-based Environment Liaison Centre International as a founder and editor of trilingual quarterly magazine, \"Ecoforum\". He later received an MSc in Science, Technology and Industrialization and a DPhil in Science and Technology Policy from the Science Policy Research Unit at the University of Sussex. He has written widely on science, technology and sustainable development.\n",
        "Section": "Early life and education"
    },
    {
        "Text": "In 1988, Professor Juma founded the African Centre for Technology Studies, Africa's first independent policy research institution designed to advance research on technology in development. In 1989 ACTS released a groundbreaking study called \"Innovation and Sovereignty\" that led to the adoption of the Industrial Property Act in Kenya and the creation of the Kenya Industrial Property Office.\n",
        "Section": "Leadership"
    },
    {
        "Text": "His continuing original work focuses on analysing the dynamics of evolutionary technological change and applying the results in advancing science and technology policy research; providing high-level science and technology advice; and promoting biodiversity conservation.\n",
        "Section": "Policy research"
    },
    {
        "Text": "Juma made significant contributions to understanding the dynamic role of technological innovation in economic transformation in developing countries. He developed the concept of \"evolutionary technological change\" to explain how socio-economic environments shape the adoption and diffusion of new technologies. This approach was elaborated in his early works such as \"Long-Run Economics: An Evolutionary Approach to Economic Growth\" (Pinter, 1987) and \"The Gene Hunters: Biotechnology and the Scramble for Seeds\" (Princeton University Press and Zed Books, 1989) and remains central to theoretical and practical work. Juma's contributions to science and technology policy focused on the role of technological innovation in sustainable development.\n",
        "Section": "Policy research::Technological innovation"
    },
    {
        "Text": "He established himself as a world leader in policy research on biotechnology and directed the International Diffusion of Biotechnology Programme of the International Federation of Institutes of Advanced Studies. He further provided international leadership in research, training and outreach through Harvard University's Kennedy School of Government and also advanced scholarship in this field as editor of the peer-reviewed \"International Journal of Technology and Globalisation\".\n",
        "Section": "Policy research::Biotechnology"
    },
    {
        "Text": "Juma contributed to biodiversity conservation in two ways. First, he helped to shape global conservation programmes during his tenure as the first permanent Executive Secretary of the Convention on Biological Diversity in Geneva and Montreal. Second, his research inspired the field of biodiplomacy that focuses on interactions between biosciences and international relations.\n",
        "Section": "Policy research::Biological diversity"
    },
    {
        "Text": "Juma's research has helped to improve understanding on the role of property rights in conservation under the rubric of \"ecological jurisprudence\" as outlined in the volume, \"In Land We Trust\" (Zed, 1996). His work guided international negotiations on the United Nations Convention on Biological Diversity (CBD) as documented in \"Biodiplomacy\" (ACTS, 1994). He later became Executive Secretary of the CBD where he advanced the use of scientific knowledge in conservation policy and practice.\n",
        "Section": "Policy research::Property rights"
    },
    {
        "Text": "Juma taught graduate courses on the role of science, technology, and innovation in development policy. The first course focused on the role of technological innovation in economic growth with emphasis on emerging regions of the world. The second course examined the policy implications of the introduction of new biotechnology products in the global economy (covering health, agriculture, industry and environment). He also taught an executive course for senior policy makers and practitioners. The Innovation executive program runs annually to provide high-level leaders from government, academia, industry, and civil society with an opportunity to learn how to integrate science and technology into a national development policy.\n",
        "Section": "Teaching"
    },
    {
        "Text": "Juma continues to provide high-level policy advice to governments, the United Nations and other international organisations on science, technology, and innovation. He chairs the Global Challenges and Biotechnology of the US National Academy of Sciences and serves as co-chair of the African High-Level Panel on Modern Biotechnology [http://www.nepadst.org/biopanel/index.shtml] of the African Union (AU) and the New Partnership for Africa's Development (NEPAD).\n",
        "Section": "Policy advice"
    },
    {
        "Text": "Juma led international experts in outlining ways to apply science and technology to the implementation of the Millennium Development Goals arising from the 2000 UN Millennium Summit. \"Innovation: Applying Knowledge in Development\" (Earthscan, 2005), the report of the Task Force on Science, Technology and Innovation of the UN Millennium Project, was released in early 2005 and its recommendations have been adopted by development agencies and governments around the world. The report has become a standard reference against which governments assess their policies and programmes on the role of technological innovation in development.\n",
        "Section": "Policy advice"
    },
    {
        "Text": "In a successor study called \"Going for Growth\", Professor Juma proposes that international development policy should be directed at building technical competence in developing countries rather than conventional relief activities. He argues that institutions of higher learning, especially universities, should be have a direct role in helping to solve development challenges.\n",
        "Section": "Policy advice"
    },
    {
        "Text": "In 2012 Juma was appointed by the African Union to chair its High-Level Panel on Science, Technology and Innovation. The report of the panel will be submitted to the AU in early 2014.\n",
        "Section": "Policy advice"
    },
    {
        "Text": "In August 2013 Monsanto approached Juma with a proposal to write a series of seven papers in support of genetically modified organisms, according to e-mails obtained through a public records request, per the Boston Globe. Monsanto suggested a headline \"Consequences of Rejecting GM crops\". In December 2014, Juma published \"Global Risks of Rejecting Agricultural Biotechnology\" on a website called \"Genetic Literacy Project, Science trumps ideology\" with the help of a Monsanto marketing firm and failed to disclose his communication with them.\n",
        "Section": "Controversy"
    }
]