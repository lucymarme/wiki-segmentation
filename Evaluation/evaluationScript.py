from transformers import glue_convert_examples_to_features as convert_examples_to_features
from transformers import RobertaTokenizer, RobertaForSequenceClassification, \
    BertTokenizer, BertForSequenceClassification
from torch.utils.data import TensorDataset, SequentialSampler, DataLoader
from transformers.data.processors.utils import InputExample
from tqdm import tqdm
import numpy as np
import pickle
import torch
import json
import sys
import os

TEST_FOLDER = "/home/lmarme/sampler_files/og-test"
# TEST_FOLDER = "./og-test"
OUT_FOLDER = "./slow-bert-og-test-results"
verbosity = 0


print(sys.argv[1:])
if len(sys.argv) < 3:
    print("Missing argument. Please provide model and model path")

# sys.argv[0] -> script-name
# roberta or bert # first argument passed in command-line
MODEL_NAME = sys.argv[1]
# ./roberta_og_consec # second argument passed in command-line
MODEL_PATH = sys.argv[2]

if len(sys.argv) > 3:
    DEVICE = sys.argv[3]
else:
    # checks if system supports CUDA (-> GPU Usage)
    DEVICE = "cuda:0" if torch.cuda.is_available() else "cpu"

if MODEL_NAME == "bert":
    MODEL = BertForSequenceClassification.from_pretrained(MODEL_PATH)
    TOKENIZER = BertTokenizer.from_pretrained(MODEL_PATH)
elif MODEL_NAME == "roberta":
    MODEL = RobertaForSequenceClassification.from_pretrained(MODEL_PATH)
    TOKENIZER = RobertaTokenizer.from_pretrained(MODEL_PATH)
MODEL.to(DEVICE)

SAME_SECTION_FLAG = "1"
DIFF_SECTION_FLAG = "0"

MAX_LENGTH = 512
MAX_BATCH_SIZE = 24


def generate_samples_per_file(file):
    with open(file) as f:
        data = json.load(f)

    examples = []
    prev_text = data[0]["Text"]
    prev_label = data[0]["Section"]
    label_list = ["0", "1"]
    output_mode = "classification"
    if len(data) < 2:
        return None

    # iterating through data dictionary
    for i, paragraph in enumerate(data[1:]):
        guid = "%s-%s" % (file, i)
        if paragraph["Section"] == prev_label:
            temp_label = SAME_SECTION_FLAG
        else:
            temp_label = DIFF_SECTION_FLAG
        # appending InputExamples with attributes which later on can be converted into input features
        # in order to be fed to the model
        examples.append(InputExample(guid=guid, text_a=prev_text,
                                     text_b=paragraph["Text"], label=temp_label))

        prev_text = paragraph["Text"]
        prev_label = paragraph["Section"]

    features = convert_examples_to_features(
        examples,
        TOKENIZER,
        label_list=label_list,
        max_length=MAX_LENGTH,
        output_mode=output_mode,
    )
    # generating dataset
    all_input_ids = torch.tensor(
        [f.input_ids for f in features], dtype=torch.long)
    all_attention_mask = torch.tensor(
        [f.attention_mask for f in features], dtype=torch.long)
    all_token_type_ids = torch.tensor(
        [f.token_type_ids for f in features], dtype=torch.long)
    all_labels = torch.tensor([f.label for f in features], dtype=torch.long)

    dataset = TensorDataset(
        all_input_ids, all_attention_mask, all_token_type_ids, all_labels)
    return dataset


def count_mistakes(ground_truth_label, model_predictions, verbosity=0):
    if verbosity >= 1:
        print("\n---------------------")
        print("Ground truth:", ground_truth_label)
        print("Predictions: ", model_predictions)
    num_mistakes = np.sum(
        np.abs(np.array(ground_truth_label) - np.array(model_predictions)))
    return num_mistakes


if __name__ == "__main__":
    prediction_dict = {}
    ground_truth_labels_dict = {}
    num_mistakes = {}
    for fn in tqdm(sorted(os.listdir(TEST_FOLDER))):
        all_preds = []
        all_labels = []
        file_dataset = generate_samples_per_file(
            os.path.join(TEST_FOLDER, fn))  # generating TensorDataset

        if not file_dataset:
            continue
        eval_sampler = SequentialSampler(file_dataset)
        # Maximize GPU usage. Datasets per file vary in length though
        # min() returns minimum value of arguments
        batch_size = min(MAX_BATCH_SIZE, len(file_dataset))
        eval_dataloader = DataLoader(
            file_dataset, sampler=eval_sampler, batch_size=batch_size)

        # Taken from run_glue's evaluate()
        for batch in eval_dataloader:
            MODEL.eval()
            batch = tuple(t.to(DEVICE) for t in batch)

            with torch.no_grad():
                inputs = {"input_ids": batch[0],
                          "attention_mask": batch[1],
                          "token_type_ids": (batch[2] if MODEL_NAME in ["bert", "xlnet", "albert"] else None),
                          "labels": batch[3]
                          }
                outputs = MODEL(**inputs)
                _, logits = outputs[:2]

            preds = logits.detach().cpu().numpy()
            out_label_ids = inputs["labels"].detach().cpu().numpy()

            preds = np.argmax(preds, axis=1)
            all_preds.append(preds.tolist())
            all_labels.append(out_label_ids.tolist())

        # adding file_name as key
        prediction_dict[fn] = all_preds[0]
        ground_truth_labels_dict[fn] = all_labels[0]
        num_mistakes_per_file = count_mistakes(all_labels[0], all_preds[0])
        num_mistakes[fn] = int(num_mistakes_per_file)

    os.makedirs(OUT_FOLDER, exist_ok=True)
    # writing all_labels to ground_truth_labels_dictionary.json
    with open(os.path.join(OUT_FOLDER, 'ground_truth_labels_dictionary.json'), 'w') as d:
        json.dump(ground_truth_labels_dict, d, indent=4)

    # writing all_preds to model_prediction_dictionary.json
    with open(os.path.join(OUT_FOLDER, 'model_prediction_dictionary.json'), 'w') as p:
        json.dump(prediction_dict, p, indent=4)

    with open(os.path.join(OUT_FOLDER, 'num_mistakes_per_file.json'), 'w') as m:
        json.dump(num_mistakes, m, indent=4)
