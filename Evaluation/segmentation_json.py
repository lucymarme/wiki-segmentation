import math
import random
import json
import sys
import os
import ast

prediction_dict = {}
ground_truth_labels_dict = {}

for files in os.listdir('/Users/lucymarme/Desktop/Fort_Praktikum/generatedSamples10000/og-test/'):
    file_name = os.fsdecode(
        '/Users/lucymarme/Desktop/Fort_Praktikum/generatedSamples10000/og-test/' + files)

    with open(file_name) as f:
        # adding file_name as key
        prediction_dict[files] = []
        ground_truth_labels_dict[files] = []

        data = json.loads(f.read())
        ground_truth_labels = []
        prev_text = data[0]['Text']
        prev_section = data[0]['Section']

        # comparing section titles
        for i in range(1, len(data)):
            if data[i]['Section'] == prev_section:
                ground_truth_labels.append(1)  # same section
            else:
                ground_truth_labels.append(0)  # different section
            prev_text = data[i]['Text']
            prev_section = data[i]['Section']

        # writing ground_truth_labels to ground_truth_labels_dictionary.json
        with open('ground_truth_labels_dictionary.json', 'w') as d:
            ground_truth_labels_dict[files] = ground_truth_labels
            json.dump(ground_truth_labels_dict, d, indent=4)

        # generating randomly one or zero and writing them to model_prediction_dictionary.json
        j = 0
        with open('model_prediction_dictionary.json', 'w') as p:
            random_value = random.random()
            rounded_value = round(random_value)
            prediction_dict.setdefault(files, []).append(rounded_value)

            for dict in data:
                random_value = random.random()
                rounded_value = round(random_value)

                if j < len(data) - 2 and rounded_value == 1:
                    prediction_dict.setdefault(
                        files, []).append(rounded_value)

                if j < len(data) - 2 and rounded_value == 0:
                    prediction_dict.setdefault(
                        files, []).append(rounded_value)
                j = j + 1
            json.dump(prediction_dict, p, indent=4)
