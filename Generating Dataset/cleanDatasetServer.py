import json
import pandas as pd
import os
from os import listdir
from os.path import isfile, join

# for files in os.listdir('/home/lmarme/csvFiles/'):
#     # for fn in files:
#     filename = os.fsdecode('/home/lmarme/csvFiles/' + files)
#     df = pd.read_csv(filename)
#     print(filename)
#     df['number of sections per document'] = ""
#     df['number of words per paragraph'] = ""

# #### discarding paragraphs with less than 11 words ####
# amount_words = []
# wordsParagraph = 0
# w = 0
# dropping_index_list = []
# for i, row in df.iterrows():
#     try:
#         if pd.isnull(df['paragraph'].iloc[i]):
#             w = 1
#         else:
#             if w != 1:
#                 amount_words_string = ""
#                 amount_words_string = df.iloc[i, 1]
#                 amount_words_temp = amount_words_string.split()

#                 if len(amount_words_temp) > 10:
#                     wordsParagraph = len(amount_words_temp)
#                     df.iloc[i, 3] = str(wordsParagraph)
#                 if len(amount_words_temp) < 11:
#                     dropping_index_list.append(i)
#             else:
#                 w = 0
#     except Exception as e:
#         pass

# ###### dropping index ######
# sorted_dropping_index_list = []
# sorted_dropping_index_list = sorted(dropping_index_list)

# for x in range(0, len(sorted_dropping_index_list)):
#     try:
#         df = df.drop(sorted_dropping_index_list[x], axis=0)
#     except Exception as e:
#         pass

# df.to_csv('/home/lmarme/cleanWordLength/CleanWordLength' +
#           files, index=False)
# del df
# m = 0
# for files in os.listdir('/home/lmarme/cleanWordLength/'):
#     m = m + 1
#     # for fn in files:
#     filename = os.fsdecode('/home/lmarme/cleanWordLength/' + files)
#     df = pd.read_csv(filename)
#     print(filename)
#     final_filename = files.split('_')[2]
#     df['number of sections per document'] = ""
#     df['number of words per paragraph'] = ""

df = pd.read_csv(
    '/Users/lucymarme/Desktop/CleanWordLengthjson_selected_data130000_TEST.csv')

#### discarding documents with only one section ####
sectionCount = 0
p = 0
s = 0
dropSection = 0
sectionString = ""
sectionList = ['startwert']
w = 0
for i, row in df.iterrows():
    try:
        if pd.isnull(df['paragraph'].iloc[i]):
            try:
                df.iloc[i - p, 2] = str(sectionCount)
            except Exception as e:
                pass
            sectionCount = 0
            p = 0
        else:
            p = p + 1
            sectionString = ""
            sectionString = df.iloc[i, 0]
            sectionList.append(sectionString)
            if sectionList[len(sectionList) - 1] != sectionList[len(sectionList) - 2]:
                sectionCount = sectionCount + 1
    except Exception as e:
        pass

    # sectionCounter = ""
    # stringOne = "1"
    # alert = 0
    # dropping_index_list = []
    # for i, row in df.iterrows():
    #     try:
    #         if pd.isnull(df['paragraph'].iloc[i]):
    #             alert = 0
    #         else:
    #             sectionCounter = ""
    #             sectionCounter = df.iloc[i, 2]
    #             if alert == 1:
    #                 dropping_index_list.append(i)
    #             if sectionCounter == stringOne:
    #                 dropping_index_list.append(i)
    #                 alert = 1
    #                 dropping_index_list.append(i - 1)
    #     except Exception as e:
    #         pass

    # ###### dropping index ######
    # sorted_dropping_index_list = []
    # sorted_dropping_index_list = sorted(dropping_index_list)

    # for x in range(0, len(sorted_dropping_index_list)):
    #     try:
    #         print(sorted_dropping_index_list[x])
    #         df = df.drop(sorted_dropping_index_list[x], axis=0)
    #     except Exception as e:
    #         pass

df.to_csv('afterCleanLength_stats_data130000' + '.csv', index=False)
# del df


# #### discarding paragraphs with less than 11 words ####
# amount_words = []
# wordsParagraph = 0
# w = 0
# dropping_index_list = []
# for i, row in df.iterrows():
#     try:
#         if pd.isnull(df['paragraph'].iloc[i]):
#             w = 1
#         else:
#             if w != 1:
#                 amount_words_string = ""
#                 amount_words_string = df.iloc[i, 1]
#                 amount_words_temp = amount_words_string.split()

#                 if len(amount_words_temp) > 10:
#                     wordsParagraph = len(amount_words_temp)
#                     df.iloc[i, 3] = str(wordsParagraph)
#                 if len(amount_words_temp) < 11:
#                     dropping_index_list.append(i)
#             else:
#                 w = 0
#     except Exception as e:
#         pass

# ###### dropping index ######
# sorted_dropping_index_list = []
# sorted_dropping_index_list = sorted(dropping_index_list)

# for x in range(0, len(sorted_dropping_index_list)):
#     try:
#         df = df.drop(sorted_dropping_index_list[x], axis=0)
#     except Exception as e:
#         pass

# df.to_csv('json_selected_data10000_CleanWordLength.csv', index=False)

# df = pd.read_csv('json_selected_data10000_CleanWordLength.csv')

# #### discarding documents with only one section ####
# sectionCount = 0
# p = 0
# s = 0
# dropSection = 0
# sectionString = ""
# sectionList = ['startwert']
# w = 0
# for i, row in df.iterrows():
#     try:
#         if pd.isnull(df['paragraph'].iloc[i]):
#             try:
#                 df.iloc[i - p, 2] = str(sectionCount)
#             except Exception as e:
#                 pass
#             sectionCount = 0
#             p = 0
#         else:
#             p = p + 1
#             sectionString = ""
#             sectionString = df.iloc[i, 0]
#             sectionList.append(sectionString)
#             if sectionList[len(sectionList) - 1] != sectionList[len(sectionList) - 2]:
#                 sectionCount = sectionCount + 1
#     except Exception as e:
#         pass

# sectionCounter = ""
# stringOne = "1"
# alert = 0
# dropping_index_list = []
# for i, row in df.iterrows():
#     try:
#         if pd.isnull(df['paragraph'].iloc[i]):
#             alert = 0
#         else:
#             sectionCounter = ""
#             sectionCounter = df.iloc[i, 2]
#             if alert == 1:
#                 dropping_index_list.append(i)
#             if sectionCounter == stringOne:
#                 dropping_index_list.append(i)
#                 alert = 1
#                 dropping_index_list.append(i - 1)
#     except Exception as e:
#         pass

# ###### dropping index ######
# sorted_dropping_index_list = []
# sorted_dropping_index_list = sorted(dropping_index_list)

# for x in range(0, len(sorted_dropping_index_list)):
#     try:
#         print(sorted_dropping_index_list[x])
#         df = df.drop(sorted_dropping_index_list[x], axis=0)
#     except Exception as e:
#         pass

# df.to_csv('json_selected_data10000_Clean.csv', index=False)
