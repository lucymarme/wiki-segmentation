import json
import pandas as pd
import os
from os import listdir
from os.path import isfile, join

#### for dataset 727 ####
onlyfiles = []
onlyfiles_integers = []
column_names = ["title", "paragraph"]
df = pd.DataFrame(columns=column_names)

i = 0
for subdir, dirs, files in os.walk('/home/lmarme/wiki_727'):
    for fn in files:
        i = i + 1
        if i > 719999:
            print(os.path.basename(os.path.join(subdir, fn)))
            onlyfiles.append(os.path.basename(
                os.path.join(subdir, fn)))
            try:
                onlyfiles_integers.append(
                    int(onlyfiles[len(onlyfiles) - 1]))
            except Exception as e:
                pass

    print(onlyfiles)

with open('wiki.txt', 'r') as t:
    h2 = []
    page_id = []
    page_title = []
    selected_page_id = []
    print(onlyfiles_integers)
    for line in t:
        try:
            if len(page_title) > 0:
                page_title = []
                page_id = []
                page_id.append(line.split(':')[1])

                if int(page_id[len(page_id) - 1]) in onlyfiles_integers:
                    selected_page_id = []
                    new_row = {'title': '', 'paragraph': ''}
                    df = df.append(new_row, ignore_index=True)
                    selected_page_id.append(page_id[len(page_id) - 1])
                    print(selected_page_id[len(selected_page_id) - 1])
                else:
                    selected_page_id = []
            if line == '\n':
                page_title = []
                page_title.append("readEmptyLine")
                h2 = []

            if len(selected_page_id) > 0 and not page_title:
                print(line)
                if "<h2>" and "<h3>" not in line:
                    if not h2:
                        new_row = {'title': [], 'paragraph': line}
                        df = df.append(new_row, ignore_index=True)
                    elif len(h2) < 2:
                        new_row = {'title': h2[0], 'paragraph': line}
                        df = df.append(new_row, ignore_index=True)
                    else:
                        row_string = ""
                        row_string += h2[0] + \
                            '::' + h2[len(h2) - 1]
                        new_row = {
                            'title': row_string, 'paragraph': line}
                        df = df.append(
                            new_row, ignore_index=True)
                if "<h2>" in line:
                    print("h2")
                    h2 = []
                    h2.append(line.split('<h2>')[0])
                    print(h2[len(h2) - 1])

                if "<h3>" in line:
                    last_element = ""
                    last_element = line.split('<h3>')[0]
                    h2.append(last_element)

        except Exception as e:
            pass

## clean dataframe ##
dropping_index_list = []
for i, row in df.iterrows():
    try:
        if "<h2>" in df.iloc[i, 1]:
            dropping_index_list.append(i)
            print("hello")
        if "<h4>" in df.iloc[i, 1]:
            dropping_index_list.append(i)
    except Exception as e:
        pass

df = df.drop(dropping_index_list, axis=0)
df.to_csv('/home/lmarme/csvFiles/json_selected_data' +
          str(i) + '_TEST.csv', index=False)
del df
column_names = ["title", "paragraph"]
df = pd.DataFrame(columns=column_names)
