import pandas as pd
import json
from pathlib import Path
import os
from os import listdir
from os.path import isfile, join
import ast

for files in os.listdir('/home/lmarme/cleanCSV/'):

    filename = os.fsdecode('/home/lmarme/cleanCSV/' + files)
    df = pd.read_csv(filename)

df = pd.read_csv(
    '/Users/lucymarme/Desktop/Fort_Praktikum/Git/data analysis/final_cleanData130000.csv')
df = df.drop('number of sections per document', 1)
df = df.drop('number of words per paragraph', 1)

page_title = []

for i, row in df.iterrows():

    if pd.isnull(df['paragraph'].iloc[i]):
        page_title = []
        page_title.append(df.iloc[i + 1, 1].split(':')[0])
        dictionary_list = []

    else:
        if "/" in page_title[0]:
            page_title[0] = page_title[0].replace("/", "||")
        file_name = page_title[0] + '.json'
        print(file_name)

        with open(file_name, 'w') as n:
            paragraph = df.iloc[i, 1]
            section = df.iloc[i, 0]
            dictionary = {"Text": str(paragraph),
                          "Section": str(section)
                          }
            dictionary_list.append(dictionary)
            json.dump(dictionary_list, n, indent=4)
