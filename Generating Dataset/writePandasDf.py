import json
import pandas as pd

with open('wiki.txt', 'r') as t:

    txt_file = t.read()
    column_names = ["title", "paragraph"]
    df = pd.DataFrame(columns=column_names)

    h2 = []
    txt_fragments = txt_file.split('\n')

    for fragment in txt_fragments:

        if "<h2>" and "<h3>" not in fragment:
            if not h2:
                new_row = {'title': [], 'paragraph': fragment}
                df = df.append(new_row, ignore_index=True)
            elif len(h2) < 2:
                new_row = {'title': h2[0], 'paragraph': fragment}
                df = df.append(new_row, ignore_index=True)
            else:
                row_string = ""
                row_string += h2[0] + ' ' + h2[len(h2) - 1]
                new_row = {'title': row_string, 'paragraph': fragment}
                df = df.append(new_row, ignore_index=True)
        if "<h2>" in fragment:
            h2 = []
            h2.append(fragment.split('<h2>')[0])

        if "<h3>" in fragment:
            last_element = ""
            last_element = fragment.split('<h3>')[0]
            h2.append(last_element)

        if fragment == '':
            h2 = []

## clean dataframe ##

dropping_index_list = []

for i, row in df.iterrows():
    try:
        if "<h2>" in df.iloc[i, 1]:
            dropping_index_list.append(i)
        if "<h4>" in df.iloc[i, 1]:
            dropping_index_list.append(i)

    except Exception as e:
        pass

df = df.drop(dropping_index_list, axis=0)

df.to_csv('json.csv', index=False)
